<?php
/*
Plugin Name: Mailing plugin
Plugin URI: https://webmenedzser.hu
Description: Mailtrap.io on localhost, Mailgun otherwise production
Version: 1.0
Author: Radics Ottó
Author URI: https://webmenedzser.hu
License: GPL2
*/

if ($_SERVER['HTTP_HOST'] == 'eltecheer.hu') {

	function phpmailer_init($phpmailer) {
		$phpmailer->isSMTP();
		$phpmailer->Host = 'smtp.mailgun.org';
		$phpmailer->SMTPAuth = true;
		$phpmailer->Port = 465;
		$phpmailer->Username = 'postmaster@eltecheer.hu';
		$phpmailer->Password = 'f8962516ec22b135293495e6f475fd90';
	}
	
	add_action('phpmailer_init', 'phpmailer_init'); 

} else {

	function mailtrap($phpmailer) {
		$phpmailer->isSMTP();
		$phpmailer->Host = 'smtp.mailtrap.io';
		$phpmailer->SMTPAuth = true;
		$phpmailer->Port = 2525;
		$phpmailer->Username = '6a6662443c1762';
		$phpmailer->Password = '4b849f0b424427';
	}

	add_action('phpmailer_init', 'mailtrap');

}