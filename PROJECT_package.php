<?php
/*
Plugin Name: PROJECT plugin
Plugin URI: https://webmenedzser.hu
Description: PROJECT plugin
Version: 1.0
Author: Radics Ottó
Author URI: https://webmenedzser.hu
License: GPL2
*/

//Dequeue Styles
function eltecheer_dequeue_unnecessary_styles() {
	// wp_dequeue_style( 'fts-feeds' );
	// wp_deregister_style( 'fts-feeds' );
}
add_action( 'wp_print_styles', 'eltecheer_dequeue_unnecessary_styles' );

//Dequeue JavaScripts
function eltecheer_dequeue_unnecessary_scripts() {
	// wp_dequeue_script( 'fts-global' );
	// wp_deregister_script( 'fts-global' );
}
add_action( 'wp_print_scripts', 'eltecheer_dequeue_unnecessary_scripts' );

?>