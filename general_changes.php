<?php
/*
Plugin Name: Webmenedzser plugin
Plugin URI: https://webmenedzser.hu
Description: Egyedi optimalizációk Wordpresshez
Version: 1.0
Author: Radics Ottó
Author URI: https://webmenedzser.hu
License: GPL2
*/

// Remove WP Version
function remove_version() {
	return '';
}

remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');

function deregister_wp_embed(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'deregister_wp_embed' );

// Remove WP Version From Styles  
add_filter( 'style_loader_src', 'remove_res_version', 9999 );

// Remove WP Version From Scripts
add_filter( 'script_loader_src', 'remove_res_version', 9999 );

// Function to remove version numbers
function remove_res_version( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}

function thumbnail_url($post_ID = 0, $size = 'medium') {
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post_ID), $size );
	$url = $thumb['0']; 

	return $url;
}

function image_url($id, $size = 'medium') {
	$image = wp_get_attachment_image_src($id, $size);
	$url = $image[0];

	return $url;
}

function data_srcset($id, $style) {
	$image_sizes = array();
	if ($style === "wide") {
		$image_sizes = array(
			"lqipWide",
			"smallWide",
			"mediumWide",
			"largeWide"
		);
	} else if ($style === "normal") {
		$image_sizes = array(
			"lqipNormal",
			"smallNormal",
			"mediumNormal",
			"largeNormal"
		);
	} else if ($style === "square") {
		$image_sizes = array(
			"lqipSquare",
			"smallSquare",
			"mediumSquare",
			"largeSquare"
		);
	}
	
	$i = 1;
	$srcset = array();

	foreach ($image_sizes as $image_size) {
		$image = wp_get_attachment_image_src($id, $image_size);
		if ($image[0] && $image[3]) {
			array_push(
				$srcset, array($image[0], $image[1])
			);
		}
	}

	$count = count($srcset);
	foreach ($srcset as $image) {
		echo $image[0];
		echo " " . $image[1] . "w";
		if ($count > $i) {
			echo ", ";
		}
		$i++;
	}
}


add_image_size('lqipNormal', 320, 180);
add_image_size('smallNormal', 640, 360);
add_image_size('mediumNormal', 1280, 720);
add_image_size('largeNormal', 2560, 1440);

add_image_size('lqipWide', 320, 128, array('center', 'center'), true);
add_image_size('smallWide', 640, 256, array('center', 'center'), true);
add_image_size('mediumWide', 1280, 512, array('center', 'center'), true);
add_image_size('largeWide', 2560, 1024, array('center', 'center'), true);

add_image_size('lqipSquare', 320, 320, array('center', 'center'), true);
add_image_size('smallSquare', 640, 640, array('center', 'center'), true);
add_image_size('mediumSquare', 1280, 1280, array('center', 'center'), true);
add_image_size('largeSquare', 2560, 2560, array('center', 'center'), true);


/*
 * custom pagination with bootstrap .pagination class
 * source: http://www.ordinarycoder.com/paginate_links-class-ul-li-bootstrap/
 */
function bootstrap_pagination( $echo = true ) {
	global $wp_query;

	$big = 999999999; // need an unlikely integer

	$pages = paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages,
			'type'  => 'array',
			'prev_next'   => true,
			'prev_text'    => __('« Prev'),
			'next_text'    => __('Next »'),
		)
	);

	if( is_array( $pages ) ) {
		$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');

		$pagination = '<ul class="pagination">';

		foreach ( $pages as $page ) {
			$pagination .= "<li>$page</li>";
		}

		$pagination .= '</ul>';

		if ( $echo ) {
			echo $pagination;
		} else {
			return $pagination;
		}
	}
}

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

?>